<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'chats_nom' => "Chats",
	'chats_slogan' => "Plugin d'apprentissage",
	'chats_description' => "Crée un nouvel objet éditorial de façon simple,
							donnant un exemple de code pour les développeurs.",
);

?>
